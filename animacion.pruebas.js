let canvas = document.querySelector("#canvas_animacion");

canvas.width = canvas.scrollWidth;
canvas.height = canvas.scrollHeight;

const img_array = [];

let spriteX = 0,
spriteY = 0,
spriteIndex = 0;

let ctx = canvas.getContext('2d');

const drawImage = (event) =>  {
    ctx.drawImage(event.target,0,0)
}

const inicializaImagenes = () => {
for (let i = 0; i < 7; i++)
{
    img_array.push(new Image());
    if (i == 6)
    {
        img_array[i].addEventListener('load', drawImage);
    }
    img_array[i].src = `sprite-frames/Frame${i}.png`; 
}
}

// keycode 37: left arrow
// keycode 39: right arrow
const keyBinding = () => {
    document.addEventListener('keydown', (e) => {
        if (e.keyCode == 37) {
            animate (-1);
            return;
        }
        if (e.keyCode == 39) {
            animate (1);
        }
    });
    document.addEventListener('keyup', (e) => {
        if (e.keyCode == 39 ) {
            ctx.clearRect(0,0,canvas.width,canvas.height);
            spriteIndex = 6;
            ctx.drawImage(img_array[spriteIndex], spriteX, spriteY ,img_array[spriteIndex].width,img_array[spriteIndex].height);  
        }
        if (e.keyCode == 37 ) {
            ctx.clearRect(0,0,canvas.width,canvas.height);
            spriteIndex = 6;
            ctx.scale(-1, 1);
            ctx.drawImage(img_array[spriteIndex], spriteX*-1, spriteY ,img_array[spriteIndex].width,img_array[spriteIndex].height);  
        }
        
    })
}

const animate = (x) => {
    let eje = 1;
    if (x > 0)
    {
        spriteIndex = (spriteIndex >= 6) ? 0 : ++ spriteIndex ;
        spriteX = (spriteX >=1000) ? 1000 : spriteX +5;

    }
    else
    {
        spriteIndex = (spriteIndex <= 0) ? 6 : -- spriteIndex ;
        spriteX = (spriteX <= 0) ? 0 : spriteX -5;
        eje = -1;
        //ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.scale(-1, 1);
        console.log ("invierto imagen");
        
    }
    ctx.clearRect(0,0,canvas.width,canvas.height);
    ctx.drawImage(img_array[spriteIndex], spriteX*eje, spriteY ,img_array[spriteIndex].width,img_array[spriteIndex].height);  

}

keyBinding();
inicializaImagenes();